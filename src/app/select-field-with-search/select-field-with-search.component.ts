import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-select-field-with-search',
  templateUrl: './select-field-with-search.component.html'
})
export class SelectFieldWithSearchComponent {
  @Input() options: Array<string>;
  
  selectedValue = null;
}
