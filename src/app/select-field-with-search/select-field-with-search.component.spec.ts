import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { SelectFieldWithSearchComponent } from './select-field-with-search.component';

describe('SelectFieldWithSearchComponent', () => {
  let component: SelectFieldWithSearchComponent;
  let fixture: ComponentFixture<SelectFieldWithSearchComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectFieldWithSearchComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SelectFieldWithSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
