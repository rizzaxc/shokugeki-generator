import { Component } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent {
  categories = ["Cuisine", "Ingredient", "Theme"];
  cuisines = ["Asian", "Italian"];
  ingredients = ["Beef", "Chicken", "Pork"];
  themes = ["Curry", "Sandwich", "Soup"];
}
